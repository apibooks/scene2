package application;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.sarxos.webcam.Webcam;

import application.mapper.MACSFaceIdentifyCandidates;
import application.mapper.MACSFaceIdentifyRequest;
import application.mapper.MACSFaceIdentifyResponse;
import application.mapper.MACSFaceImformation;
import application.mapper.MACSFaceRectangle;
import application.mapper.MACSPerson;
import application.mapper.MACSPersonGroup;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;

public class FaceRecognitionController implements Initializable {
  @FXML
  private TextField txtFaceAPIURL;
  @FXML
  private TextField txtAPIKey;
  @FXML
  private TextField txtPersonGroupName;
  @FXML
  private TextField txtPersonGroupId;
  @FXML
  private TextField txtPersonName;
  @FXML
  private Canvas canvas;
  @FXML
  private ComboBox<Dimension> cmbSizes;
  @FXML
  private ComboBox<MACSPersonGroup> cmbPersonGroupForCreatePerson;
  @FXML
  private ComboBox<MACSPerson> cmbPersonForAddFace;
  @FXML
  private ComboBox<MACSPersonGroup> cmbPersonGroupForTrain;
  @FXML
  private ComboBox<MACSPersonGroup> cmbPersonGroupForAddFace;
  @FXML
  private ComboBox<MACSPersonGroup> cmbPersonGroupForIdentify;
  @FXML
  private Button btnCaptureStartStop;
  @FXML
  private Button btnCapture;

  private Webcam webcam;

  private GraphicsContext gc;

  private Font font = Font.font("Meiryo UI", FontWeight.NORMAL,
      FontPosture.REGULAR, 12);

  private DateTimeFormatter timestamp = DateTimeFormatter
      .ofPattern("yyyy/MM/dd,E,HH,mm");

  private DateTimeFormatter timecardName = DateTimeFormatter
      .ofPattern("yyyyMMdd");

  private Map<String, MACSPersonGroup> personGroups = new HashMap<>();

  private boolean disposed;

  private boolean stopRefresh;

  private byte[] imageInByte;

  private Callback<ListView<Dimension>, ListCell<Dimension>> cameraSizeCallback = (
      ListView<Dimension> param) -> new ListCell<Dimension>() {
        @Override
        protected void updateItem(Dimension item, boolean empty) {
          super.updateItem(item, empty);
          if (item != null && !empty) {
            setText(item.width + "," + item.height);
          }
        }
      };

  private Callback<ListView<MACSPersonGroup>, ListCell<MACSPersonGroup>> personGroupCallback = (
      ListView<MACSPersonGroup> param) -> new ListCell<MACSPersonGroup>() {
        @Override
        protected void updateItem(MACSPersonGroup item, boolean empty) {
          super.updateItem(item, empty);
          if (item != null && !empty) {
            setText(item.name);
          }
        }
      };

  private Callback<ListView<MACSPerson>, ListCell<MACSPerson>> personCallback = (
      ListView<MACSPerson> param) -> new ListCell<MACSPerson>() {
        @Override
        protected void updateItem(MACSPerson item, boolean empty) {
          super.updateItem(item, empty);
          if (item != null && !empty) {
            setText(item.name);
          }
        }
      };

  public void onCameraStartStop() {
    if (webcam.isOpen()) {
      webcam.close();
      imageInByte = null;
      gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
      btnCaptureStartStop.setText("Camera Start");
      return;
    }
    Dimension d = cmbSizes.getSelectionModel().getSelectedItem();
    if (d == null) {
      showMessage("Select Web camera size");
      return;
    }
    webcam.setViewSize(d);
    webcam.open();
    btnCaptureStartStop.setText("Camera Stop");
  }

  public void onCapture() throws IOException {
    if (!webcam.isOpen()) {
      showMessage("Start Web camera");
      return;
    }
    if (stopRefresh) {
      stopRefresh = false;
      imageInByte = null;
      btnCapture.setText("Capture");
      return;
    }
    stopRefresh = true;
    BufferedImage capture = webcam.getImage();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ImageIO.write(capture, "png", baos);
    baos.flush();
    imageInByte = baos.toByteArray();
    baos.close();
    btnCapture.setText("Cancel");
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    String url = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/";
    String key = "2ed9e170cc3d4bbca4d7d55ae343cdb2";
    txtFaceAPIURL.setText(url);
    txtAPIKey.setText(key);

    gc = canvas.getGraphicsContext2D();
    webcam = Webcam.getDefault();
    if (webcam == null) {
      showMessage("Web Camera is not found.");
      return;
    }
    Dimension[] webCamSizes = webcam.getViewSizes();
    ObservableList<Dimension> list = FXCollections
        .observableArrayList(webCamSizes);
    cmbSizes.setItems(list);
    cmbSizes.setButtonCell(cameraSizeCallback.call(null));
    cmbSizes.setCellFactory(cameraSizeCallback);

    cmbPersonGroupForCreatePerson.setButtonCell(personGroupCallback.call(null));
    cmbPersonGroupForCreatePerson.setCellFactory(personGroupCallback);

    cmbPersonGroupForTrain.setButtonCell(personGroupCallback.call(null));
    cmbPersonGroupForTrain.setCellFactory(personGroupCallback);

    cmbPersonGroupForAddFace.setButtonCell(personGroupCallback.call(null));
    cmbPersonGroupForAddFace.setCellFactory(personGroupCallback);

    cmbPersonGroupForIdentify.setButtonCell(personGroupCallback.call(null));
    cmbPersonGroupForIdentify.setCellFactory(personGroupCallback);

    cmbPersonForAddFace.setButtonCell(personCallback.call(null));
    cmbPersonForAddFace.setCellFactory(personCallback);

    Thread thread = new Thread(new Runnable() {
      @Override
      public void run() {
        while (!disposed) {
          try {
            if (stopRefresh || !webcam.isOpen()) {
              Thread.sleep(1000);
              continue;
            }
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          BufferedImage capture = webcam.getImage();
          Image image = SwingFXUtils.toFXImage(capture, null);
          gc.drawImage(image, 0, 0);
        }
      }
    });
    thread.start();
  }

  public void dispose() {
    disposed = true;

  }

  public void refreshPerson()
      throws URISyntaxException, ClientProtocolException, IOException {
    MACSPersonGroup g = cmbPersonGroupForAddFace.getSelectionModel()
        .getSelectedItem();
    if (g == null) {
      return;
    }
    refreshPerson(g);
  }

  public void refreshPerson(MACSPersonGroup g)
      throws URISyntaxException, ClientProtocolException, IOException {
    HttpClient httpclient = HttpClients.createDefault();
    String url = txtFaceAPIURL.getText() + "persongroups/" + g.personGroupId
        + "/persons";
    System.out.println(url);
    URIBuilder builder = new URIBuilder(url);
    HttpGet request = new HttpGet(builder.build());
    request.setHeader("Ocp-Apim-Subscription-Key", txtAPIKey.getText());
    HttpResponse response = httpclient.execute(request);
    HttpEntity entity = response.getEntity();
    String jsonString = EntityUtils.toString(entity).trim();
    showMessage(jsonString);

    ObjectMapper mapper = new ObjectMapper();
    MACSPerson[] info = mapper.readValue(jsonString, MACSPerson[].class);

    MACSPersonGroup selectedGroup = personGroups.get(g.personGroupId);

    Arrays.asList(info).stream()
        .filter(p -> !selectedGroup.persons.containsKey(p.personId))
        .forEach(p -> selectedGroup.persons.put(p.personId, p));

    updateData();
  }

  public void onGetPersonGroup()
      throws URISyntaxException, ClientProtocolException, IOException {
    HttpClient httpclient = HttpClients.createDefault();
    String url = txtFaceAPIURL.getText() + "persongroups";
    System.out.println(url);
    URIBuilder builder = new URIBuilder(url);
    HttpGet request = new HttpGet(builder.build());
    request.setHeader("Ocp-Apim-Subscription-Key", txtAPIKey.getText());
    HttpResponse response = httpclient.execute(request);
    HttpEntity entity = response.getEntity();
    String jsonString = EntityUtils.toString(entity).trim();
    showMessage(jsonString);

    ObjectMapper mapper = new ObjectMapper();
    MACSPersonGroup[] info = mapper.readValue(jsonString,
        MACSPersonGroup[].class);

    Arrays.asList(info).stream()
        .filter(p -> !personGroups.containsKey(p.personGroupId))
        .forEach(p -> personGroups.put(p.personGroupId, p));

    updateData();
  }

  private void updateData() {
    ObservableList<MACSPersonGroup> list = FXCollections
        .observableArrayList((personGroups.values()));
    cmbPersonGroupForCreatePerson.setItems(list);
    cmbPersonGroupForTrain.setItems(list);
    cmbPersonGroupForAddFace.setItems(list);
    cmbPersonGroupForIdentify.setItems(list);

    MACSPersonGroup g = cmbPersonGroupForAddFace.getSelectionModel()
        .getSelectedItem();
    if (g != null) {
      ObservableList<MACSPerson> personList = FXCollections
          .observableArrayList((g.persons.values()));
      cmbPersonForAddFace.setItems(personList);
    }
  }

  public void onCreatePersonGroup()
      throws URISyntaxException, ClientProtocolException, IOException {
    HttpClient httpclient = HttpClients.createDefault();
    String url = txtFaceAPIURL.getText() + "persongroups/"
        + txtPersonGroupId.getText();
    URIBuilder builder = new URIBuilder(url);
    HttpPut request = new HttpPut(builder.build());
    request.setHeader("Ocp-Apim-Subscription-Key", txtAPIKey.getText());
    request.setHeader("Content-Type", "application/json");
    String content = String.format("{\"name\":\"%s\"}",
        txtPersonGroupName.getText());
    HttpEntity reqEntity = new StringEntity(content);
    request.setEntity(reqEntity);
    HttpResponse response = httpclient.execute(request);
    HttpEntity entity = response.getEntity();
    String jsonString = EntityUtils.toString(entity).trim();
    showMessage(jsonString);

    // refresh person group
    onGetPersonGroup();
  }

  public void onCreatePerson()
      throws URISyntaxException, ClientProtocolException, IOException {
    HttpClient httpclient = HttpClients.createDefault();
    MACSPersonGroup g = cmbPersonGroupForCreatePerson.getSelectionModel()
        .getSelectedItem();
    if (g == null) {
      return;
    }
    String url = txtFaceAPIURL.getText() + "persongroups/" + g.personGroupId
        + "/persons";
    URIBuilder builder = new URIBuilder(url);
    HttpPost request = new HttpPost(builder.build());
    request.setHeader("Ocp-Apim-Subscription-Key", txtAPIKey.getText());
    request.setHeader("Content-Type", "application/json");
    String content = String.format("{\"name\":\"%s\"}",
        txtPersonName.getText());
    HttpEntity reqEntity = new StringEntity(content);
    request.setEntity(reqEntity);
    HttpResponse response = httpclient.execute(request);
    HttpEntity entity = response.getEntity();
    String jsonString = EntityUtils.toString(entity).trim();
    showMessage(jsonString);

    // refresh
    onGetPersonGroup();
  }

  public void onAddFace()
      throws URISyntaxException, ClientProtocolException, IOException {
    MACSPersonGroup g = cmbPersonGroupForAddFace.getSelectionModel()
        .getSelectedItem();
    if (g == null) {
      showMessage("Select PersonGroup");
      return;
    }
    MACSPerson p = cmbPersonForAddFace.getSelectionModel().getSelectedItem();
    if (p == null) {
      showMessage("Select Person");
      return;
    }
    if (imageInByte == null) {
      showMessage("Capture your face");
      return;
    }

    String url = txtFaceAPIURL.getText() + "persongroups/" + g.personGroupId
        + "/persons/" + p.personId + "/persistedFaces";
    HttpClient httpclient = HttpClients.createDefault();
    URIBuilder builder = new URIBuilder(url);
    HttpPost request = new HttpPost(builder.build());
    request.setHeader("Ocp-Apim-Subscription-Key", txtAPIKey.getText());
    request.setHeader("Content-Type", "application/octet-stream");

    HttpEntity reqEntity = new ByteArrayEntity(imageInByte);
    request.setEntity(reqEntity);

    HttpResponse response = httpclient.execute(request);

    HttpEntity entity = response.getEntity();
    String jsonString = EntityUtils.toString(entity).trim();
    showMessage(jsonString);
  }

  public void onTrain()
      throws URISyntaxException, ClientProtocolException, IOException {
    MACSPersonGroup g = cmbPersonGroupForTrain.getSelectionModel()
        .getSelectedItem();
    if (g == null) {
      showMessage("Select PersonGroup");
      return;
    }
    String url = txtFaceAPIURL.getText() + "persongroups/" + g.personGroupId
        + "/train";
    HttpClient httpclient = HttpClients.createDefault();
    URIBuilder builder = new URIBuilder(url);
    HttpPost request = new HttpPost(builder.build());
    request.setHeader("Ocp-Apim-Subscription-Key", txtAPIKey.getText());

    HttpResponse response = httpclient.execute(request);

    HttpEntity entity = response.getEntity();
    String jsonString = EntityUtils.toString(entity).trim();
    showMessage(jsonString);
  }

  public MACSFaceIdentifyResponse[] onIdentify()
      throws URISyntaxException, ClientProtocolException, IOException {
    if (imageInByte == null) {
      showMessage("Capture your face");
      return null;
    }
    String url = txtFaceAPIURL.getText() + "detect";
    HttpClient httpclient = HttpClients.createDefault();
    URIBuilder builder = new URIBuilder(url);
    HttpPost request = new HttpPost(builder.build());
    request.setHeader("Ocp-Apim-Subscription-Key", txtAPIKey.getText());
    request.setHeader("Content-Type", "application/octet-stream");
    HttpEntity reqEntity = new ByteArrayEntity(imageInByte);
    request.setEntity(reqEntity);
    HttpResponse response = httpclient.execute(request);
    HttpEntity entity = response.getEntity();
    String jsonString = EntityUtils.toString(entity).trim();
    ObjectMapper mapper = new ObjectMapper();
    MACSFaceImformation[] info = mapper.readValue(jsonString,
        MACSFaceImformation[].class);
    return identifyPerson(info);
  }

  public void onPunchAClock() throws IOException, URISyntaxException {
    MACSPersonGroup g = cmbPersonGroupForIdentify.getSelectionModel()
        .getSelectedItem();
    if (g == null) {
      showMessage("Select PersonGroup");
      return;
    }
    onCapture();
    MACSFaceIdentifyResponse[] identifyInfo = onIdentify();

    if (identifyInfo == null) {
      showMessage("Cannot identify");
      onCapture();
      return;
    }

    List<Optional<MACSFaceIdentifyCandidates>> calist = Arrays
        .stream(identifyInfo)
        .map(i -> Arrays.stream(i.candidates)
            .max((c1, c2) -> Float.compare(c1.confidence, c2.confidence)))
        .collect(Collectors.toList());

    LocalDateTime date = LocalDateTime.now();
    List<String> list = new ArrayList<>();
    calist.forEach(of -> of.ifPresent(fi -> {
      MACSPerson p = g.persons.get(fi.personId);
      list.add(p.name + "," + timestamp.format(date));

    }));
    String charset = "MS932";
    Path path = Paths.get("timecard" + timecardName.format(date) + ".csv");
    if (Files.notExists(path)) {
      Files.createFile(path);
    }
    Files.write(path, list, Charset.forName(charset),
        StandardOpenOption.APPEND);

    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        try {
          Thread.sleep(3000);
          onCapture();
        } catch (IOException | InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }

      }
    });

  }

  private MACSFaceIdentifyResponse[] identifyPerson(
      MACSFaceImformation[] faceInfo)
      throws URISyntaxException, ClientProtocolException, IOException {
    MACSPersonGroup g = cmbPersonGroupForIdentify.getSelectionModel()
        .getSelectedItem();
    if (g == null) {
      showMessage("Select PersonGroup");
      return null;
    }
    refreshPerson(g);
    List<String> faceIds = Arrays.stream(faceInfo).map((i) -> i.faceId)
        .collect(Collectors.toList());

    String url = txtFaceAPIURL.getText() + "identify";
    HttpClient httpclient = HttpClients.createDefault();
    URIBuilder builder = new URIBuilder(url);
    HttpPost request = new HttpPost(builder.build());
    request.setHeader("Ocp-Apim-Subscription-Key", txtAPIKey.getText());
    request.setHeader("Content-Type", "application/json");

    MACSFaceIdentifyRequest requestContent = new MACSFaceIdentifyRequest();
    requestContent.faceIds = faceIds.toArray(new String[faceIds.size()]);
    requestContent.personGroupId = g.personGroupId;
    ObjectMapper mapper = new ObjectMapper();
    String content = mapper.writeValueAsString(requestContent);
    HttpEntity reqEntity = new StringEntity(content);
    request.setEntity(reqEntity);
    HttpResponse response = httpclient.execute(request);

    HttpEntity entity = response.getEntity();
    String jsonString = EntityUtils.toString(entity).trim();
    showMessage(jsonString);
    System.out.println(jsonString);

    MACSFaceIdentifyResponse[] identifyInfo = mapper.readValue(jsonString,
        MACSFaceIdentifyResponse[].class);

    Map<String, StringBuilder> faceMap = new HashMap<>();

    Arrays.stream(identifyInfo).forEach(i -> {
      StringBuilder data = new StringBuilder();
      faceMap.put(i.faceId, data);
      Arrays.stream(i.candidates).forEach(c -> {
        if (data.length() > 0) {
          data.append(",");
        }
        MACSPerson p = g.persons.get(c.personId);
        data.append(p.name);
        data.append("(");
        data.append(c.confidence);
        data.append(")");
      });
    });

    Arrays.stream(faceInfo).forEach(i -> {
      StringBuilder data = faceMap.get(i.faceId);
      MACSFaceRectangle r = i.faceRectangle;
      String name = data.toString();
      gc.setStroke(Color.RED);
      gc.setLineWidth(5);
      gc.strokeRect(r.left, r.top, r.width, r.height);

      gc.setLineWidth(1);
      gc.setFont(font);
      gc.strokeText(name, r.left, r.top - 10);

    });
    return identifyInfo;

  }

  private void showMessage(String message) {
    System.out.println(message);

  }

}
