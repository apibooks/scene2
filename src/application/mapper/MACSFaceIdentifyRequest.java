package application.mapper;

public class MACSFaceIdentifyRequest {
	public String[] faceIds;
	public String personGroupId;
	public String largePersonGroupId;
	public Integer maxNumOfCandidatesReturned;
	public Float confidenceThreshold;
}
